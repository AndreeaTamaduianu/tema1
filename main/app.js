
function distance(first, second) {

    
        if(!Array.isArray(first) || !Array.isArray(second))
        {
            throw new Error('InvalidType')
        }
        else {
            if(!first.length || !second.length) return 0;
            else
            {
                var result = [];
                first=[...new Set(first)]
                second=[...new Set(second)]
                for (var i = 0; i < first.length; i++) {
                  if (second.indexOf(first[i]) === -1) {
                    result.push(first[i]);
                  }
                }
                for (i = 0; i < second.length; i++) {
                  if (first.indexOf(second[i]) === -1) {
                    result.push(second[i]);
                  }
                }

                return result.length
            }
        }

  }
  

module.exports.distance = distance